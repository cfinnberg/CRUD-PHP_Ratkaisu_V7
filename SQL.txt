CREATE DATABASE IF NOT EXISTS PHPesimerkki;

CREATE TABLE `PHPesimerkki`.`users` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(50) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `PHPesimerkki`.`list` ( 
    `id` INT NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    `details` TEXT NOT NULL,
    `timestamp_posted` TIMESTAMP NOT NULL DEFAULT NOW(),
    `timestamp_edited` TIMESTAMP NULL DEFAULT NULL ON UPDATE NOW(),
    `public` TINYINT(1) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB;