<?php
    session_start();
    session_destroy();

    // Redirect to homepage
    header("location:index.php");
?>