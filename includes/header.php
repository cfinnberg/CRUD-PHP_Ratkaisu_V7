<?
session_start(); //starts the session
if (isset($_SESSION['user'])) { //checks if user is logged in
    $user = $_SESSION['user']; //assigns user value
    $user_id = $_SESSION['user_id'];
} else if (isset($LOGIN_REQUIRED)) {
    header("location:login.php"); // redirects if user is not logged in
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= WEBSITE_TITLE . " | $PAGE_TITLE" ?></title>
    <style>
    h2 {
        text-align: center;
    }
    table {
        border: 1px solid black;
        width: 100%;
    }
    td {
        text-align: center;
    }
    nav {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    .links a {
        font-size: 1.5rem;
        text-decoration: none;
        color: gray;
        margin-left: 2rem;
    }
    .links a.active {
        color: black;
        font-weight: bold;
        cursor: default;
    }
    .links a:hover:not(.active) {
        color: green;
    }
    .links p {
        display: inline-block;
    }
    .error_msg {
        color: red;
    }
    .logo {
        text-decoration: none;
        color: black;
        font-size: 3rem;
    }
    </style>
</head>
<body>

<div>
    <nav>
        <a class="logo" href="index.php"><?= WEBSITE_TITLE ?></a>
        <div class="links">
            <? if (isset($user)) { ?>
                <p>Hello <?= "$user" ?>!</p> <!--Displays user's name-->
            <? } ?>
            <a <?= $PAGE_TITLE=='Home'?'class="active"':'href="index.php"'?>>Home</a>
            <? if (isset($user)) { ?>
                <a href="logout.php">Logout</a>
            <? } else { ?>
                <a <?= $PAGE_TITLE=='User Login'?'class="active"':'href="login.php"'?>>Login</a>
                <a <?= $PAGE_TITLE=='User registration'?'class="active"':'href="register.php"'?>>Register</a>
            <? } ?>
        </div>
    </nav>
    <h1><?= $PAGE_TITLE ?></h1>
</div>
