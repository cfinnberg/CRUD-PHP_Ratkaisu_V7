<?php
// Prepare settings
require_once('config.php');
$PAGE_TITLE = 'Edit';
$LOGIN_REQUIRED = TRUE;

if ($_SERVER['REQUEST_METHOD'] == "POST") {
	require('connect_database.php');

	// Sanitize input
	$details = mysqli_real_escape_string($link, $_POST['details']);
	$list_id = $_POST['list_id'];

	// Update data in database
	mysqli_query($link, "UPDATE list SET details='$details', public='" . isset($_POST['public']) . "' WHERE id='$list_id'") ;
	mysqli_close($link);

	header("location: index.php");
}

// Get request

// Page header
include('includes/header.php');
?>

<h2>Currently Selected</h2>
<table>
	<tr>
		<th>Id</th>
		<th>Details</th>
		<th>Post Time</th>
		<th>Edit Time</th>
		<th>Public Post</th>
	</tr>
	<?php
		if(!empty($_GET['id']))
		{
			// Get id from address parameters
			$id = $_GET['id'];

			require('connect_database.php');
			
			// Query database
			$query = mysqli_query($link, "Select id, details, UNIX_TIMESTAMP(timestamp_posted) as timestamp_posted, UNIX_TIMESTAMP(timestamp_edited) as timestamp_edited, public from list Where user_id='$user_id' and id='$id'");

			// Go through the results
			if (mysqli_num_rows($query) > 0) {
				$row = mysqli_fetch_array($query, MYSQLI_ASSOC);
				$public = $row['public']?' checked ':''; ?>
				<form action="edit.php" method="POST">
				<tr>
					<td><?= $row['id'] ?></td>
					<td><input type="text" name="details" value="<?= $row['details'] ?>"/></td>
					<td><?= strftime(DATE_FORMAT,$row['timestamp_posted'])?></td>
					<td><?= strftime(DATE_FORMAT,$row['timestamp_edited'])?></td>
					<td><input type="checkbox" name="public" value="yes"<?= $public ?>/></td>
				</tr><br>
				<input type="hidden" id="list_id" name="list_id" value="<?= $id ?>">
				<input type="submit" value="Update List"/></form>
			<? } else { ?>
				<h2>There is no data to be edited.</h2>
			<? }
			mysqli_close($link);
		} ?>
</table>
<br/>

<? include('includes/footer.php');
