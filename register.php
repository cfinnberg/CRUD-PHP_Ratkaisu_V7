<?
// Prepare settings
require_once('config.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
    /*****************************
     * Sanitize and escape input *
     *****************************/

    // Connect to the database
    require('connect_database.php');
    
    // Get username and password from form.
    $username = strtolower(mysqli_real_escape_string($link, $_POST["username"]));
    $password = mysqli_real_escape_string($link, $_POST["password"]);

    /******************************
     * Check for input for errors *
     ******************************/

    // Checks if username exists
    $query = mysqli_query($link, "Select * from users where username='$username'");
    if (mysqli_num_rows($query) > 0) {
        $error_msg = "Username is taken!";
    }

    // TODO: Check if user is too short?
    // TODO: Retype password?


    /*************************************************
     * Save username and password if no input errors *
     *************************************************/
    if (!isset($error_msg)) {
        // Hash (encrypt) password
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        // And save username and password in the database
        mysqli_query($link, "INSERT INTO users (username, password) VALUES ('$username','$hashed_password')");

        // Get the user we just saved to get the user id
        $query = mysqli_query($link, "Select * from users where username='$username'");
        $row = mysqli_fetch_array($query, MYSQLI_ASSOC);

        // Close connection to the database
        mysqli_close($link);

        // Log in user
        session_start();
        $_SESSION['user'] = $username;
        $_SESSION['user_id'] = $row['id'];

        // Redirect to the homepage
        header('location:index.php');
    }
}

// If not POST or if registration failed, show the form
$PAGE_TITLE = 'User registration';
include('includes/header.php');

// Show errors if any
if (isset($error_msg)) { ?>
    <div class="error_msg"><p><?= $error_msg ?></p></div>
<? } 

// Show registration form
?>
<form action="register.php" method="POST">
    Enter Username: <input type="text" name="username" required="required" /> <br>
    Enter password: <input type="password" name="password" required="required" /> <br>
    <input type="submit" value="Register">
</form>

<? require('includes/footer.php');
