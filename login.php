<?
// Prepare settings
require_once('config.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    /*****************************
     * Sanitize and escape input *
     *****************************/
    // Connect to the database
    require('connect_database.php');

    // Get username and password from form.
    $username = strtolower(mysqli_real_escape_string($link, $_POST["username"]));
    $password = mysqli_real_escape_string($link, $_POST["password"]);

    /******************************
     * Check for input for errors *
     ******************************/
    // Checks if username exists and is the correct password
    $query = mysqli_query($link, "Select * from users where username='$username'");
    if (mysqli_num_rows($query) == 0) {
        $error_msg = "Incorrect username or password!";
    } else {
        $row = mysqli_fetch_array($query, MYSQLI_ASSOC);
        if (!password_verify($password, $row['password'])) {
            $error_msg = "Incorrect username or password!";
        }
    }

    /****************************
     * Log in user if no errors *
     ****************************/
    if (!isset($error_msg)) {

        // Close connection to the database
        mysqli_close($link);

        // Log in user
        session_start();
        $_SESSION['user'] = $username;
        $_SESSION['user_id'] = $row['id'];

        // Redirect to the homepage
        header('location:index.php');
    }
}

// If not POST or if login failed, show the form
$PAGE_TITLE = 'User Login';
include('includes/header.php');

// Show errors if any
if (isset($error_msg)) { ?>
    <div class="error_msg"><p><?= $error_msg ?></p></div>
<? } 

// Show login form
?>
<form action="" method="POST">
    Enter Username: <input type="text" name="username" required="required"> <br>
    Enter password: <input type="password" name="password" required="required"> <br>
    <input type="submit" value="Login">
</form>

<? require('includes/footer.php');
