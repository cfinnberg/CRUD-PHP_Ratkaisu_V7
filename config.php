<?php

# Database settings
define('DB_SERVER','localhost');
define('DB_PORT', 3306);
define('DB_NAME', 'PHPesimerkki');
define('DB_USER','root');
define('DB_PASSWORD','');

# Password settings
define("SALT_LENGTH", 16);
define("ITERATIONS", 1000);

# Misc settings
date_default_timezone_set("Europe/Helsinki");
setlocale(LC_TIME, "fi");
define('DATE_FORMAT',"%A %d.%m.%Y - %H:%M:%S");

# Website settings
define('WEBSITE_TITLE', 'My first PHP Website');
define('ROOT_FOLDER', 'php_Esimerkki');