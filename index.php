<?
// Prepare settings
require_once('config.php');
$PAGE_TITLE = 'Home';

// Page header
include('includes/header.php');

// If $user is set, a user is logged in. He/She can make some actions.
if (isset($user)) { ?>
    <form action="add.php" method="POST">
        Add more to list: <input type="text" name="details"/><br/>
        public post? <input type="checkbox" name="public" value="yes"/><br/>
        <input type="submit" value="Add to list"/>
    </form>
<? } ?>

<h2>List</h2>
<table>
    <tr>
        <th>Id</th>
        <th>Details</th>
        <th>Post Time</th>
        <th>Edit Time</th>
        <? if (isset($user)) { ?>
            <th>Edit</th>
            <th>Delete</th>
            <th>Public Post</th>
        <? } else { ?>
            <th>User</th>
        <? } ?>
    </tr>
    <?
    // Connect to server
    require('connect_database.php');

    // Execute SQL query
    $query = "Select list.id, details, UNIX_TIMESTAMP(timestamp_posted) as timestamp_posted, UNIX_TIMESTAMP(timestamp_edited) as timestamp_edited, public, username from list inner join users on list.user_id = users.id";
    if (!isset($user)) {
        $query .= " where public=TRUE";
    }
    $query = mysqli_query($link, $query);
    
    // Go through the results
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) { ?>
        <tr>
            <td><?= $row['id'] ?></td>
            <td><?= $row['details'] ?></td>
            <td><?= strftime(DATE_FORMAT,$row['timestamp_posted']) ?></td>
            <td><?= ($row['timestamp_edited']===NULL?'-':strftime(DATE_FORMAT,$row['timestamp_edited'])) ?></td>
            <? if (isset($user)) { ?>
                <td><a href="edit.php?id=<?= $row['id'] ?>">edit</a></td>
				<td><a href="#" onclick="myFunction(<?= $row['id'] ?>)">delete</a></td>
				<td><?= ($row['public']?'Yes':'No') ?></td>
            <? } else { ?>
                <td><?= $row['username'] ?></td>
            <? } ?>
        </tr>
    <? } ?>
</table>

<? if (isset($user)) { ?>
    <script>
        function myFunction(id) {
            if (confirm("Are you sure you want to delete this record?")) {
            window.location.assign("delete.php?id=" + id);
            }
        }
    </script>
<? } ?>

<?
require('includes/footer.php');
