<?php
    session_start();
    if (isset($_SESSION['user'])) { //checks if user is logged in
        $user = $_SESSION['user']; //assigns user value
        $user_id = $_SESSION['user_id'];
    }
    else{
        header("location:login.php"); // redirects if user is not logged in
    }

    if($_SERVER['REQUEST_METHOD'] != "POST") { //Added an if to keep the page secured
        header("location:index.php"); //redirects back to home
    }

    require('config.php');
    $link = mysqli_connect(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME, DB_PORT) or die (mysql_error()); //Connect to server
    $details = mysqli_real_escape_string($link, $_POST['details']);
    $res = mysqli_query($link, "INSERT INTO list (user_id, details, public) VALUES ('$user_id', '$details', '" . isset($_POST['public']) . "')"); //SQL query
    mysqli_close($link);
    header("location: index.php");
?>