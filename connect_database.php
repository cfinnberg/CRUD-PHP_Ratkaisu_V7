<?
require_once('config.php');

// Check if connection to database is already open
if (!isset($link)) {
    // Connect to the database
    $link = mysqli_connect(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME, DB_PORT) or die(mysqli_connect_error());
}
